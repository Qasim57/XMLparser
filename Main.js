var XMLstring = '\
<store attr1="booga" attr2="boo">\
  <id>123</id>\
  <name>My Store</name>\
	<books>\
	  <book>\
	    <title>Book1</title>\
	    <author>Author1</author>\
	  </book>\
	  <book>\
	    <title>Book2</title>\
	    <author>Author2</author>\
	  </book>\
	</books>\
</store>';

//Generic object for XML nodes, one instance is created for each node we've got
var XMLNode = function(nodeName, nodeParent, nodeValue, nodeAttributes, nodeChildren){
	//Instantiating with provided values, OR blank values
	this.name = nodeName || "";
	this.parent = nodeParent || undefined;
	this.value = nodeValue || "";
	this.attributes = nodeAttributes || [];
	this.children = nodeChildren || [];

	xmlNodes.push(this);//Adding this to the xmlNodes collection

	this.getNodeName = function(){ return this.name }
	this.getNodeValue = function(){ return this.value == "" ? this.children : this.value }
	this.getParentNode = function(){ return this.parent }
	this.getChildNodes = function(){ return this.children }
	this.getAttributes = function(){ return this.attributes }

	//Search attributes of current node
	this.getAttributeByName = function(attributeName){
		for(var i = 0; i < this.attributes.length; i++)
		{
			if(this.attributes[i][0] === attributeName)
				return this.attributes[i][1];//Returning the value of this attribute
		}
	}

	//Search all elements by tag-name
	this.getElementsByTagName = function(tagName){
		var results = [];
		for(var i = 0; i < xmlNodes.length; i++)
		{
			if(xmlNodes[i].name === tagName)
				results.push(xmlNodes[i])
		}
		return results;
	}

	//Add child to current-node
	this.appendChild = function(xmlNode){
		this.children.push(xmlNode);
	}

	//Remove child from current-node
	this.removeChild = function(xmlNode){
		for(var i = 0; i < this.children.length; i++)
		{
			if(this.children[i] === xmlNode)
			{
				this.children.splice(i, 1);//removing 1 element, from index i
				xmlNodes.splice(xmlNodes.indexOf(xmlNode), 1);//Removing element from xmlNodes collection
			}

		}
	}	
}//xmlNode obj ENDs

var xmlNodes = [];//Collection of all xmlNode objects. Used to getElementByTagName
var xmlDoc = new XMLParser(XMLstring);//XMLParser() returns root xmlNode obj in XMLstring


//Processes XML tree recursively, and provides methods for navigating, reading vals, attributes, etc
function XMLParser(xmlStr){

	var rootNode = new XMLNode();//This xmlNode object, is the ancestor of all created xmlNodes
	parseNode(xmlStr, rootNode);//Parse xmlStr, and create the resultant node

	return rootNode.children[0];//returns the first child-node of the XML tree

	//Takes an XML string and xmlNode parent. returns {nodeName, [attribute key-val 2D array], and "nodeContent"}
	function parseNode(xmlNodeStr, parent)
	{
		//Outputs of this function. Current node's name, [attributes], and "contents"
		var nodeName, nodeAttributes = [], nodeContent;

		//vars used to navigate around node data
		var openAngleBracket = xmlNodeStr.indexOf('<'),//Index of < and >, in the opening XML tag
		closeAngleBracket = xmlNodeStr.indexOf('>'),
		nodeTag = xmlNodeStr.substring(openAngleBracket + 1, closeAngleBracket),//Points to inner-contents of XML opening tag. openAngleBracket + 1 omits the starting '<'
		spaceChar = nodeTag.indexOf(' '),//IF this nodeTag has attributes, it'd have atleast one ' ' space(between nodeName and attr key-val pair/s)
		resultNode,	//the xmlNode Object we populate, with contents/attributes/children, of this node
		nodeClosingTagIndex,//Points to where the containing node tag ends(it's closing tag's </)
		nextSiblingStartIndex,//When we have sibling nodes, this points to their starting index
		nextSiblingString;//Sibling substring

		//IF we didn't find any opening '<' or closing '>' brackets, it means we've got no sibling or descendent nodes. So we return
		if(openAngleBracket === -1 && closeAngleBracket === -1)
		{
			parent.value = xmlNodeStr;
			return;
		}else{
			resultNode = new XMLNode();
		}
		//if((openAngleBracket + nodeTag.length + 1) === closeAngleBracket)
		//Check if this node has attributes(atleast one spaceChar)
		if(spaceChar !== -1)
		{
			nodeName = nodeTag.substring(0, spaceChar);

			var attributes = nodeTag.substring(spaceChar + 1).split(' ');//Starting at first space character, then splitting on spaces between attributes
			for(var i = 0; i < attributes.length; i++)
			{
				//Check for empty whitespace at end of XML tag
				if(attributes[i] !== "")
				{
					attributekeyValPair = attributes[i].split('=');
					nodeAttributes.push([attributekeyValPair[0], attributekeyValPair[1].substring(1, attributekeyValPair[1].length - 1)]);//removing extra " " from value(substring)
				}
			}
		}else{
			//We have no attributes -- nodeTag consists solely of the name of the XML tag
			nodeName = nodeTag;
		}

		//Calculating node content
		nodeClosingTagIndex = xmlNodeStr.indexOf(nodeName, closeAngleBracket) - 2;//In </store> closing-tag, - 2 removes '</' chars
		nodeContent = xmlNodeStr.substring(closeAngleBracket + 1, nodeClosingTagIndex);

		//The result-node object for this node
		resultNode.name = nodeName;
		resultNode.attributes = nodeAttributes;
		resultNode.parent = parent;
		parent.children.push(resultNode);//Add this resultNode to the array of children, of it's parent
		// if(nodeName === "id")
		// {
			// debugger;
		// }
		//Recursively parse child-nodes
		parseNode(nodeContent, resultNode);

		//Recursively parse sibling-nodes(if present)
		nextSiblingStartIndex = xmlNodeStr.indexOf('<', nodeClosingTagIndex + 1);//We skip the '<' of our current closing-node, and look for more nodes('<')

		if(nextSiblingStartIndex !== -1){
			nextSiblingString = xmlNodeStr.substring(nextSiblingStartIndex);
			parseNode(nextSiblingString, parent)//Parse sibling-nodes
		}
		

		// this.name = nodeName || "";
		// this.parent = nodeParent || undefined;
		// this.attributes = nodeAttributes || [];
		// this.children = nodeChildren || [];

		//return 
	}//parseNode() ENDs
}//XMLParser() ENDs